CARGO ?= cargo
EXEC ?= mic

.DEFAULT_GOAL := release

.PHONY: release
release:
	$(CARGO) build -r
	cp target/release/$(EXEC) ./

.PHONY: run
code_formatting:
	$(CARGO) fmt --all

.PHONY: check
check:
	$(CARGO) clippy
