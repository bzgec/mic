/*
 * https://askubuntu.com/a/1152002/1546799
 *   pacmd list-sources | \
 *           grep -oP 'index: \d+' | \
 *           awk '{ print $2 }' | \
 *           xargs -I{} pactl set-source-mute {} toggle \
 *           && pacmd list-sources | \
 *           grep -oP 'muted: (?:yes|no)'
 */

use std::process::{Command, Stdio};
use std::str;

use clap::Parser;

mod cli;

/// Program used to send desktop notifications
const NOTIPY_PROGRAM: &str = "notify-send";

fn main() {
    let cli = cli::Cli::parse();

    match &cli.command {
        cli::Commands::State => state_cmd(),
        cli::Commands::On(args) => on_cmd(args),
        cli::Commands::Off(args) => off_cmd(args),
        cli::Commands::Toggle(args) => toggle_cmd(args),
    }
}

/// Execute `state` subcommand
fn state_cmd() {
    println!("{}", get_mic_state());
}

/// Execute `on` subcommand
fn on_cmd(args: &cli::ArgsOnOff) {
    set_mic_state("0");
    if args.notify {
        push_state_notification(get_mic_state());
    }
}

/// Execute `off` subcommand
fn off_cmd(args: &cli::ArgsOnOff) {
    set_mic_state("1");
    if args.notify {
        push_state_notification(get_mic_state());
    }
}

/// Execute `toggle` subcommand
fn toggle_cmd(args: &cli::ArgsToggle) {
    let mic_state: &str = get_mic_state();

    if mic_state != "ON" && mic_state != "OFF" {
        set_mic_state("1"); // 1 is OFF
    } else {
        set_mic_state("toggle");
    }

    let mic_state: &str = get_mic_state();

    if args.notify {
        push_state_notification(mic_state);
    }

    println!("{}", mic_state);
}

/// Push microphone state with desktop notification program
fn push_state_notification(mic_state: &str) {
    Command::new(NOTIPY_PROGRAM)
        .args(["Microphone state", mic_state])
        .output()
        .expect("Failed to execute `notify-send` command");
}

// TODO: use Result as return value?
/*
 * Get microphone state
 *
 * Command:
 *   pacmd list-sources | \
 *       grep -oP 'muted: (?:yes|no)'
 */
fn get_mic_state() -> &'static str {
    // https://doc.rust-lang.org/std/process/index.html#handling-io
    // stdout must be configured with `Stdio::piped` in order to use
    // `pacmd_child.stdout`
    let pacmd_child = Command::new("pacmd")
        .arg("list-sources")
        .stdout(Stdio::piped())
        .spawn()
        .expect("Failed to start `pacmd` process");

    // Note that `pacmd_child` is moved here, but we won't be needing
    // `pacmd_child` anymore
    let pacmd_out = pacmd_child.stdout.expect("Failed to open `pacmd` stdout");

    let grep_child = Command::new("grep")
        .args(["-oP", "muted: (?:yes|no)"])
        .stdin(Stdio::from(pacmd_out))
        .stdout(Stdio::piped())
        .spawn()
        .expect("Failed to start `grep` process");

    let output = grep_child
        .wait_with_output()
        .expect("Failed to wait on `grep`");
    let mut result = str::from_utf8(&output.stdout).unwrap();
    result = result.trim();
    //println!("Result: {}", result);

    // TODO: find better solution
    let contains_yes: bool = result.contains("yes");
    let contains_no: bool = result.contains("no");

    if contains_yes && contains_no {
        // Some microphones are muted, some are not
        "ON-OFF"
    } else if contains_yes && !contains_no {
        // All microphones are muted
        "OFF"
    } else if !contains_yes && contains_no {
        // All microphones are NOT muted
        "ON"
    } else {
        // Bad output
        "Bad output"
    }
}

/*
 * Set new microphone state, possible states are 0 , 1 and 'toggle'.
 * Note that 0 is unmute, and 1 is mute.
 *
 * Command:
 *   pacmd list-sources | \
 *           grep -oP 'index: \d+' | \
 *           awk '{ print $2 }' | \
 *           xargs -I{} pactl set-source-mute {} NEW_STATE
 */
fn set_mic_state(new_state: &str) {
    let pacmd_child = Command::new("pacmd")
        .arg("list-sources")
        .stdout(Stdio::piped())
        .spawn()
        .expect("Failed to start `pacmd` process");

    let pacmd_out = pacmd_child.stdout.expect("Failed to open `pacmd` stdout");

    let grep_child = Command::new("grep")
        .args(["-oP", "index: \\d+"])
        .stdin(Stdio::from(pacmd_out))
        .stdout(Stdio::piped())
        .spawn()
        .expect("Failed to start `grep` process");

    let grep_out = grep_child.stdout.expect("Failed to open `grep` stdout");

    let awk_child = Command::new("awk")
        .arg("{ print $2 }")
        .stdin(Stdio::from(grep_out))
        .stdout(Stdio::piped())
        .spawn()
        .expect("Failed to start `awk` process");

    let awk_out = awk_child.stdout.expect("Failed to open `awk` stdout");

    Command::new("xargs")
        .args(["-I{}", "pactl", "set-source-mute", "{}", new_state])
        .stdin(Stdio::from(awk_out))
        .output()
        .expect("Failed to start `xargs` process");
}
