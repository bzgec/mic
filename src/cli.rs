use clap::{Args, Parser, Subcommand};

/// Set/get state of all microphones
#[derive(Parser)]
#[command(author, version, about, long_about = None)]
pub struct Cli {
    #[command(subcommand)]
    pub command: Commands,
}

#[derive(Subcommand)]
pub enum Commands {
    /// Get state of all microphones
    State,

    /// Toggle all microphones
    Toggle(ArgsToggle),

    /// Turn ON all microphones
    On(ArgsOnOff),

    /// Turn OFF all microphones
    Off(ArgsOnOff),
}

#[derive(Args)]
pub struct ArgsToggle {
    /// Return new microphone state and send desktop notification
    #[arg(short = 'n', long = "notify", default_value_t = false)]
    pub notify: bool,
}

#[derive(Args)]
pub struct ArgsOnOff {
    /// Send desktop notification
    #[arg(short = 'n', long = "notify", default_value_t = false)]
    pub notify: bool,
}
